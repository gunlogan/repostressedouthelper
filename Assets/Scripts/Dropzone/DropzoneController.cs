﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropzoneController : MonoBehaviour
{
    //Serialized Fields
    [SerializeField] private Color _red, _green, _yellow;
    
    // Private members
    private Dropzone[] _zones;
    List<PresentColor> _colors;


    private void Awake()
    {
        // Assign values to list
        _colors = new List<PresentColor>()
        {
            PresentColor.Green,
            PresentColor.Red,
            PresentColor.Yellow
        };
    }
    
    private void OnDisable()
    {
        foreach (var zone in _zones)
        {
            zone.CorrectPresentRecieved -= OnValidPresent;
            zone.InvalidPresentRecieved -= OnInvalidPresent;
        }
    }

    private void OnEnable()
    {
        _zones = GetComponentsInChildren<Dropzone>();
        foreach (var zone in _zones)
        {
            zone.CorrectPresentRecieved += OnValidPresent;
            zone.InvalidPresentRecieved += OnInvalidPresent;
        }
    }

    /// <summary>
    /// Raranges a list of type E
    /// </summary>
    /// <typeparam name="E">The type of list</typeparam>
    /// <param name="inputList">The input list</param>
    /// <returns>The output list</returns>
    private List<E> ShuffleList<E>(List<E> inputList)
    {
        List<E> randomList = new List<E>();

        System.Random r = new System.Random();
        int randomIndex = 0;
        while (inputList.Count > 0)
        {
            randomIndex = r.Next(0, inputList.Count); //Choose a random object in the list
            randomList.Add(inputList[randomIndex]); //add it to the new, random list
            inputList.RemoveAt(randomIndex); //remove to avoid duplicates
        }

        return randomList; //return the new random list
    }

    private void ChangeColor(Dropzone[] zones)
    {
        _colors = ShuffleList(_colors);
        for (int i = 0; i < _colors.Count; i++)
        {
            zones[i].Color = _colors[i];
        }
    }

    public void OnValidPresent(PresentColor color, PresentShape shape)
    {
        Debug.Log("Recieved a " + color + " " + shape + "! ");

        ChangeColor(_zones);               
    }

    private void OnInvalidPresent()
    {
        Debug.LogError("Invalid present recieved");
    }

}
