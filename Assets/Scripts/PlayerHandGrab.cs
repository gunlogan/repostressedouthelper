﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHandGrab : MonoBehaviour
{
    public GameObject _christmasPresent;
    public GameObject _playerHand;
    public float _playerHandPower;

    [SerializeField] private PresentSpawner _spawner;

    bool _inPlayerHand = false;
    Collider _presentCol;
    Rigidbody _presentRb;
    Camera _cam;


    // Start is called before the first frame update
    void Start()
    {
        _presentCol = _christmasPresent.GetComponent<BoxCollider>();
        _presentRb = _christmasPresent.GetComponent<Rigidbody>();
        _cam = GetComponentInChildren<Camera>();
    }

    private void OnEnable()
    {
        _spawner.OnSpawnPresent += SetCorrectGrabObject;
    }

    private void SetCorrectGrabObject(GameObject obj)
    {
        _christmasPresent = obj;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            if (!_inPlayerHand) {
                _presentCol = _christmasPresent.GetComponent<Collider>();
                _presentRb = _christmasPresent.GetComponent<Rigidbody>();
                _presentCol.isTrigger = true;
                _christmasPresent.transform.SetParent(_playerHand.transform);
                _christmasPresent.transform.localPosition = new Vector3(0f, -1f, 2.48f);
                _presentRb.velocity = Vector3.zero;
                _presentRb.useGravity = false;
                _inPlayerHand = true;
            } else if (_inPlayerHand) 
            {
                _presentCol = _christmasPresent.GetComponent<Collider>();
                _presentRb = _christmasPresent.GetComponent<Rigidbody>();
                _presentCol.isTrigger = false;
                _presentRb.useGravity = true; 
                this.GetComponent<PlayerHandGrab>().enabled = false;
                _christmasPresent.transform.SetParent(null);
                _presentRb.velocity = _cam.transform.rotation * Vector3.forward * _playerHandPower;  
                _inPlayerHand = false;
            }
        }

    }


    private void OnDisable()
    {
        _spawner.OnSpawnPresent -= SetCorrectGrabObject;
    }
}
