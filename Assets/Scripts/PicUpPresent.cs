﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PicUpPresent : MonoBehaviour
{
    GameObject _mainCamera;
    public bool _carrying;
    GameObject _carriedObject;
    public float _distance;
    public float _smooth;

    // Start is called before the first frame update
    void Start() {
        _mainCamera = GameObject.FindWithTag("MainCamera");
    }

    // Update is called once per frame
    void Update() {
        if(_carrying) {
            _carry(_carriedObject);
        } else {
            _pickUp();
        }
    }

    void _carry(GameObject o) {
        o.transform.position = Vector3.Lerp (o.transform.position, _mainCamera.transform.position + _mainCamera.transform.forward * _distance, Time.deltaTime * _smooth);
    }

    void _pickUp() {
        if(Input.GetButtonDown("Fire1")) {
            int x = Screen.width / 2;
            int y = Screen.height / 2;

            Ray ray = _mainCamera.GetComponent<Camera>().ScreenPointToRay(new Vector3(x, y));
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit)) {
                PickUpable p = hit.collider.GetComponent<PickUpable>();
                if (p != null) {
                    _carrying = true;
                    _carriedObject = p.gameObject;
                    p.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                }
            }
        }
    } 

    void _checkDrop() {
        if(Input.GetButtonDown ("Fire1")) {
            _dropObject();
        }
    }

    public void _dropObject() {
        _carrying = false;
        _carriedObject.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        _carriedObject = null;
    }
}
