﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MaterialEmitterController : MonoBehaviour
{
    [SerializeField] private PresentSpawner _spawner;
   

    private ButtonType[] buttons;

    private void Start()
    {
        buttons = FindObjectsOfType<ButtonType>();
        foreach (var btn in buttons)
        {
            btn.OnBtnClick += OnEmit;
        }
    }

    private void OnEmit(PresentShape shape, PresentColor color)
    {
        //Debug.Log("Creating a " + color.ToString() + " " + shape.ToString());
        _spawner.SpawnPresent(shape, color);
    }

    private void OnDisable()
    {
        foreach (var btn in buttons)
        {
            btn.OnBtnClick -= OnEmit;
        }
    }
}


public enum PresentShape
{
    Box,
    Envelope,
    Caramell
}

public enum PresentColor
{
    Red,
    Green,
    Yellow
}