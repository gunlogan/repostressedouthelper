﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ButtonType : MonoBehaviour
{
    [SerializeField] private PresentColor _color;
    [SerializeField] private PresentShape _shape;

    public PresentColor Color { get => _color; }
    public PresentShape Shape { get => _shape; }

    public Action<PresentShape, PresentColor> OnBtnClick;

    public void OnClick()
    {
        OnBtnClick?.Invoke(Shape, Color);
    }
}
